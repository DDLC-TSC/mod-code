## This is an example scene
    ## It teaches you a little about making mods
    
    # Each section needs a label, this is how we will call the scene in other parts
    # of the script
    
    #“Sup?” (Neutral)
label NR1:
    "Aaron looks up at me and gives a small wave"

    "Monika smiles at us both"

    m "Now, before everyone takes up their usual activities-Aaron, would you tell us a bit about yourself?"

    "Aaron turns his attention back to the floor, as if he is trying to think of something to say..."

    a "Well, I can’t really say I’m an interesting kinda guy…"

    n "Really? No Interesting qualities whatsoever?"

    a "Nothing that comes to mind...I usually just spend my alone time gaming or doing stuff online in all honesty..."

    m "It’s fine, Aaron. Not everyone has to have exciting lives..."

    m "Well guys, you're all free to do whatever you’d like today as long as it’s literature related."
    
    m "But before you do anything, I will let you guys know that before the club session is over, I’ll be giving out an assignment that will be mandatory for everyone here."

    "Everyone breaks up and starts doing their own thing, Monika sits at the Teacher’s desk reading a book,"
    
    "Yuri and Sayori sit together near the back of the classroom talking about a new book Yuri bought last week,"
    
    "Natsuki is reading her Manga in the far corner of the room and Aaron is writing in a hardcover book."

    "...Anywho, I should probably see what everyone’s up to since I don’t really have anything to read… Who should I check up on first?"
    menu:

        "Monika":
            "I approach Monika, who seems to be deeply invested in her book..."

            mc  "Hey Monika, whatcha reading?"

            "She flinches a bit, I guess she didn’t hear me walk up to her..."

            m "Ah- oh… sorry about that, [player]... I'm just reading this romance novel… It’s nothing much to talk about, really, it’s actually not as interesting…"

            mc "Bored out of your mind?"

            m "Yeah… this is bit too generic for my tastes… anywho, what do you think about him?"

            mc "Who? Aaron? He seems to be pretty alright. Do you think he’ll make a good addition to the club?"
    
            m "I hope so, He seems nice but seems really shy..."

            mc "Yea, Especially after Natsuki got on his case..."

            m "Well, Hopefully that attitude won’t last. I want everyone here to get along after all..."

            mc "I agree, I'm gonna see what everyone else is up to…"

            m "Ok, thanks for talking!"

        "Yuri and Sayori":
            "I make my way to Yuri and Sayori, who are talking about some book on the desk."

            mc  "Hey, you two. What’s going on here?"

            s "Oh!  Hey [player]!  Yuri here is telling me all about this new book!"

            "The book has the silhouette of a bird of some sort engraved into the cover, with the French Flag superimposed behind the bird outline."

            mc "Well, what’s it called?"

            y "Oh, it’s called ‘Day of the Crows’."

            mc "Cool. What’s it about?"

            s "I dunno, that’s why I’m over here!"

            y "Why, I’m glad you asked [player]. Essentially, the book goes like this;"
            
            y "a man in 17th century France finds himself accused of breaching the law, and so he becomes a part of this gang."

            mc "Sounds interesting."

            s "I think so!"

            y "So far it seems to be a parody-type novel.  Think Don Quixote meets a resistance plot."

            "Sayori and I both give Yuri confused looks. Aaron’s head momentarily pops out of his book."

            y "You don’t know who that is, do you."

            s "Nope! Sounds like it’s fun to read though, Yuri!"

            "Yuri shrugs, setting the book in her lap."

            y "Well, it’s mostly a comedy based narrative, but it’s a small break from my horror novels."

            mc "Well, you two have fun, I'm gonna see what everyone else is up to..."

            s "Have fun!"

        "Aaron":
            "I walk over to Aaron who seems deeply invested in whatever he’s writing about…"


            mc "Hey Aaron, mind if we talk for a bit?"

            "Aaron suddenly jumps as he looks over at me."

            a "...O-oh, hey [player]...sure…"

            "Aaron slips his pen into his book and closes it."

            mc "So what do you think about the club so far?"

            a "I’ll be honest, I’m not too sure yet… I’ve never been apart of any real clubs before so I can’t really compare this to anything…"

            mc "Don’t worry man, today might’ve been a bit stressful since it’s the first day of University as well as the first day of the club making a comeback."

            a "Oh yea, Monika did mention you guys all knew each other since last year at your high school… I do feel a bit bad intruding in an otherwise well established group…"

            mc "Natsuki didn’t say that to you, did she?"

            a "N-no… she hasn’t said a word to me since we introduced each other… I just feel like I’m just inserting myself in something I shouldn’t even be in…"

            mc "Don’t feel bad, we are all glad you’re here. Even Natsuki is, you just have to give her some time to adjust…"

            a "I see… thank you [player]..."


        "Natsuki":

            "I walk over to Natsuki, Reading the latest Issue of  Parfait Girl’s, Even though she seems to be enjoying her read, I still sense there is some tension within her..."

            mc "Hey Nat, how’s this issue so far?"

            n "Hey, so far it’s nothing but filler… This series has been going downhill as of recent..."

            mc "Really? I wish I kept up with the series..."

            n "Let's cut to the chase [Player], I know you're going to ask me about him."

            "She read me like a book"

            n "As far as how I see him, I’m not impressed. He seems like a complete loser if you ask me."

            mc "You didn’t like me very much when I first joined last year...."

            n "Well that’s different, you seemed lazy back then. Aaron however just seems…weird…"

            mc "Not everyone thinks the same Natsuki, for example, Yuri doesn’t believe Manga is literature…"

            n "BUT IT IS…"

            mc "I’m not saying it isn’t, my point is she thinks differently and yet you still get along with her right?"

            N "... Alright. Fine. I’ll give Aaron a chance…"

            mc "Good to hear, let me know when your issue starts getting good…"

            "She smirks at my remark as I walk away..."

    "After checking on everyone, I return to my seat and do some reading required for my Literature class until Monika stands up and stops us from everything we’re doing..."

    m "Ok everyone! we’re almost about done for today, If we can all come to the center of the room, I will explain everyone’s assignment for tonight…"

    "Everyone including myself now stands in front of Monika, awaiting what our assignment will be, but part of me has a hunch on what it will be..."

    m "Now for most of you here, you’ll remember that during our first week of the club last year we all did poetry as a way for all of us to get to know each other better."

    m "Since we have Aaron here this year, I figured now’s the best time to take another whack at it again, so tonight you all have to write a poem and share it with everyone tomorrow!"

    s "Woohoo! Poems! It’s been forever since we last did them!"

    a "Poems huh? I’ll try my best but I can’t guarantee I’ll be any good."

    y "You don’t need to worry Aaron, everyone has to start from somewhere, and not everyone can make a masterpiece at their first attempt."

    n "At least make sure yours are decent, [player] made some really trashy ones last year, even read one of his in front of a whole audience while they all fell asleep."


    
    "Natsuki looks me straight in the eyes as she tries her hardest to get under my skin, I know for a fact my earlier poems weren’t THAT bad…"


    m "Just try your best Aaron, matter of fact here’s some advice…"
    m "When it comes to poetry or writing in general, it’s always best to write things your passionate about. A hobby or something you like for example."
    m "If you don’t write about something you particularly like, you can’t guarantee you’ll put as much effort into it and your work may suffer because of it."
    m "So in the end, make sure you have fun when your writing about anything. That’s Monika’s writing tip of the day!"
    m "I’ll see you all tomorrow!"

    "We exchange our goodbyes and go home, hopefully everyone writes good poems for tomorrow!"

    #poem game

    #call NR2

