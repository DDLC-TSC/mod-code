## This is an example scene
    ## It teaches you a little about making mods
    
    # Each section needs a label, this is how we will call the scene in other parts
    # of the script
    
    #“Sup?” (Neutral)
label BR1:
    $ nat_gf = 0
    a "Oh, sorry, I'm just a bit nervous..."

    "Monika looks at me with a disappointed face, like she’s not sure what I said."

    mc "So, Aaron, what courses are you taking?"

    a "I’m taking Engineering."

    "I chuckle to myself quietly."

    "Aaron shots me a quizzical look."

    "The chuckles grow into fully grown laughter, sharply resonating around the room."

    "Aaron looks embarrassed."

    "Most of the club is awkwardly standing around, waiting for me to finish in an uncomfortable silence."
    
    "I, however, couldn't hold it in anymore I fall into hysterics, nearly unable to keep myself stable with my own two feet."

    a "uhm is something wrong?"

    "I pause in my amusement to haphazardly respond."

    mc "No, no! I just didn't think Americans did that kinda stuff."

    "I slip back into my laughter, coming out hard and bitter. The mood of the room begins to pitch, the tension in the air palpable and clear."

    "Monika shoots me an angry glare, catching me a little off guard."

    "I stop laughing, and allow a serious look to cast over my face."

    m "Okay, that's enough [player]. With Aaron people might take us more seriously and consider us more diverse. That might even attract more members."

    n "Please, no more boys…"

    y "Now Natsuki, don't you think that more boys could be a good thing?"

    n "They tend to kill the mood."
    
    s "boys can be good.. as long as they aren't meanies!"
    
    m "We should accept all new members, every race or gender!"

    menu:
        "Who should I side with..."

        "Natsuki":
            mc "I kinda agree with Natsuki"
            mc "we can't really trust anyone until we know them"

        "Yuri":

            mc "Yuri is right, more boys in the club can be good. It might give me and Aaron more things to talk about. Especially guy stuff…"

            s "Ewwww, I don’t want to intrude in your conversations!"

            mc "aha, just kidding"

        "Sayori":
            mc "I agree sayori as long as they aren't big meanies, they're cool in my book."

        "Monika":
            mc "Yeah guys we should accept everyone. Getting new members is a good thing right?"


    "Natsuki gave me a quick glance, before letting her gaze return back to Yuri."
    
    "Everyone’s eyes are darting around the room, seeing who’ll lash out first. Sayori tries to do her usual thing of placating everyone."

    s "Hey guys…"

    "She doesn’t finish the thought, as everyone’s eyes trained on her. The tension got to her, and she stepped back. Monika quickly intervened before a verbal argument breaks loose."

    m "Ok everyone! That brings an abrupt ending to today's official first meeting! I hope to see you all tomorrow."

    s "I’m hungry."

    y "Me too, I could go for some food."

    n "I’ve got some leftovers at my place, I can just eat those."

    a "I don't really know my way around this town. I'll look around a bit, and see if I can find a fast food joint."

    "I mumble under my breath, as to not get everyone angry again."

    mc "Doesn’t seem like you need more fast food."

    "Monika looks over at me, a funny look on her face."

    "Oh crap, she heard."

    "Everyone piles out the door one by one until I'm about to walk out. Then, I hear Monika call out after me right before I exit."

    m "Can you come here for a moment, [player]?"

    "Her tone was dripping with some unhappy emotion. Frustration? Anger? Disgust? Either way I relent, turning heel and coming back into the room."
    
    "Monika steps past me, darting her arm out and pulling the door shut.  She counts out for three seconds on her fingers, then wheels around on me."

    m "WHAT WAS THAT!?!"

    "I momentarily pause, unsurprised and unfazed by Monika’s outburst."

    mc "I was just speaking my mind…"

    m "Then try not to be such a…"

    "Monika takes a deep breath, then continues."

    m "Look, we both care about this club, right? I say that we treat all new potential members with respect. Alright?"

    "Monika motions me to leave. I oblige her and leave in a huff."

    "About 15 minutes later I arrive at my favorite place to eat since I was a kid. They were good times hanging with Sayori."

    "I walk in only to see none other than Aaron..."

    mc "Hey Aaron"

    a "... H-hey [player]"

    "He looks worried"

    m "Hey what are you ordering"

    a "Just some fries and a large soda"

    mc "Odd time to be thinking of your weight now…"

    "I whisper to myself."

    "Aaron looks down embarrassed."

    a "so what are you getting?"

    mc "I'm probably just gonna get a burger or something"

    "After we order we both head back to our respective dorms."

    "I sit down and get comfortable, ready to binge watch my favorite anime."

    "I hear my phone ring.."

    m "Hey, it's Monika. I forgot to tell you In order to help out Aaron with learning our language better, we are all gonna write poems and present them tomorrow."

    mc "Okay then."

    "I hang up"

    "Well, I thought I was going to relax tonight but now I have to write another poem."

    "Let’s let the thoughts start flowing! I hope..."

    #poem game

    #call BR2

